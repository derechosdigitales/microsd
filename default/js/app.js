$(document).foundation()

// Set links href
//$(location).attr('host');
//$("#en").attr("href", window.location.origin + "/microsd/en/");
$("#en").attr("href", "../en/index.html");
$("#pt").attr("href", "../pt/index.html");
$("#es").attr("href", "../es/index.html");

// Reset collection on laguage change
$( "#en" ).click(function() { localStorage.clear(); });
$( "#es" ).click(function() { localStorage.clear(); });
$( "#pt" ).click(function() { localStorage.clear(); });


//   Obtención de parametros GET
$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results !== null){
		return results[1] || 0;
	} else {
		return 0;
	};
};

function text_truncate(str) {
	length = 400;
	ending = '...';
	if (str.length > length) {
		return str.substring(0, length - ending.length) + ending;
	} else {
		return str;
	}
};

function popup_tarjeta(tarjeta){
	var contenido_tarjeta = "";
	var imagen_tarjeta = "";
	var relaciones_tarjeta = "";
	var coleccion = JSON.parse(localStorage.getItem("coleccion"));
	var tarjeta_seleccionada = coleccion[tarjeta-1][tarjeta];
	if (tarjeta_seleccionada.seleccionada == 0) {
		clase_icono = "fi-heart favorite";
	} else {
		clase_icono = "fi-heart favorite-selected";
	};
	contenido_tarjeta += "<span class=\"titulo-carta\">"+ tarjeta_seleccionada["titulo"] + "</span><i onclick=\"cerrar_modal();\" class=\"fi-x icono_cerrar\"></i><i onclick=\"marcar_tarjeta("+ tarjeta +");\" class=\""+ clase_icono +" icono-favorito marcar_"+ tarjeta +"\"></i>\n\n" +"<p class=\"texto-azul descripcion-carta-modal\">"+ tarjeta_seleccionada["descripción"] + "</p>";
	relaciones_tarjeta += "";
	tarjetas_asociadas_l = String(tarjeta_seleccionada["relaciones"]);
	tarjetas_asociadas = tarjetas_asociadas_l.split(",");
	for (var i in tarjetas_asociadas) {
    //console.log(tarjetas_asociadas[parseInt(i)]);
    numero_tarjeta = parseInt(tarjetas_asociadas[parseInt(i)])
    var obj = datos.tarjetas[numero_tarjeta-1];
    // console.log(numero_tarjeta);
    for ( var key in obj) {
    	relaciones_tarjeta += "<div class='card-2 carta-"+ obj[key].tipo_carta +"' onclick='popup_tarjeta("+ key +");'><img src='img/th"+ obj[key].arte +"'>"
    	+ "<div class='card-section'>"
    	+ "<p class='titulo-carta-relacionada'>"+ obj[key].titulo +"</p>"
    	+ "</div>"
    	+ "</div>";
    }
  }
  $('#carta_modal').attr('class', 'large reveal');
  $('#carta_modal').addClass('carta-'+tarjeta_seleccionada.tipo_carta);
  $('.reveal-overlay').addClass('reveal-'+tarjeta_seleccionada.tipo_carta);
  imagen_tarjeta += "<img src='img/" + tarjeta_seleccionada["arte"] + "'>"
  $("#imagen_carta_modal").html(imagen_tarjeta);
  $("#contenido_carta_modal").html(contenido_tarjeta);
  $("#relaciones_carta_modal").html(relaciones_tarjeta);
  $('#carta_modal').foundation('open');
};

function cerrar_modal() {
	$('#carta_modal').foundation('close');
}

// Inicio Manejo de colecciones
function chequear_coleccion_local() {
	if (localStorage.getItem("coleccion") === null) {
		// console.log("No existe variable local de colección... Creando... ");
		var coleccion = datos.tarjetas;
		for (var i = 0; i < coleccion.length; i+=1) {
			coleccion[i][i+1].seleccionada = 0;
			// console.log("En el índice '" + (i+1) + "' hay este valor: " + coleccion[i][i+1].titulo + " y seleccionada = " + coleccion[i][i+1].seleccionada);
		}
		localStorage.setItem("coleccion", JSON.stringify(coleccion));
		//console.log("Creada la variable de colección ");
	}
	else {
		//console.log("Ya existía una variable local de colección, continuando...");
	}
};

function mostrar_badge() {
	var coleccion = JSON.parse(localStorage.getItem("coleccion"));
	var tarjetas_en_coleccion = 0;
	for (var i = 0; i < coleccion.length; i+=1) {
		if (coleccion[i][i+1].seleccionada == 1){
			tarjetas_en_coleccion++;
		}
	}
	$("#badge_coleccion").html(tarjetas_en_coleccion);
	if (tarjetas_en_coleccion == 0) {
		$("#li_coleccion").hide();
	} else {
		$("#li_coleccion").show();
	}
	// console.log("Hay " + tarjetas_en_coleccion + " que están seleccionadas");
};

function marcar_tarjeta(tarjeta) {
	var coleccion = JSON.parse(localStorage.getItem("coleccion"))
	if (coleccion[tarjeta-1][tarjeta].seleccionada == 0) {
		// console.log("La tarjeta " + tarjeta + " no estaba marcada, marcando...");
		coleccion[tarjeta-1][tarjeta].seleccionada = 1;
		$(".marcar_"+tarjeta).removeClass("favorite");
		$(".marcar_"+tarjeta).addClass("favorite-selected");
	} else if (coleccion[tarjeta-1][tarjeta].seleccionada == 1) {
		// console.log("La tarjeta " + tarjeta + " ya estaba marcada, desmarcando...");
		coleccion[tarjeta-1][tarjeta].seleccionada = 0;
		$(".marcar_"+tarjeta).removeClass("favorite-selected");
		$(".marcar_"+tarjeta).addClass("favorite");
	}
	localStorage.setItem("coleccion", JSON.stringify(coleccion));
	mostrar_badge();
};

function mostrar_coleccion(){
	var mostrar_tarjetas_asociadas = "";
	var tarjetas_asociadas = JSON.parse(localStorage.getItem("coleccion"));
	for (var i in tarjetas_asociadas) {
		if (tarjetas_asociadas[i][parseInt(i)+1].seleccionada == 1) {
			numero_tarjeta = parseInt(i)+1;
  	 //console.log(numero_tarjeta);
  	 var obj = tarjetas_asociadas[numero_tarjeta-1];
  	 for ( var key in obj) {
  	 	if (obj[key].seleccionada == 0) {
  	 		clase_icono = "fi-star";
  	 	} else {
  	 		clase_icono = "fi-check";
  	 	};
  	 	mostrar_tarjetas_asociadas += "<div class='small-12 carta carta-"+ obj[key].tipo_carta + "'"
  	 	+""
  	 	+ ">"
  	 	+ "<div class=\"grid-x  grid-margin-x\"><div class=\"cell small-12 large-6\">"
  	 	+ "<img class=\"img-coleccion\" src='img/"+ obj[key].arte +"' onclick=\"popup_tarjeta("+ key +");\"></div>"

  	 	+ "<div class=\"small-12 large-6 coleccion-descripcion\">"
  	 	+ "<span class='numero-carta-coleccion' >"+ key +"</span>"
  	 	+ "<p class='titulo-carta-coleccion'>"+ obj[key].titulo +"</p>"
  	 	+ "<p class='descripcion-carta-coleccion'>"+ obj[key].descripción +"</p>"
  	 	+ "<div class='cartas-relacionadas-coleccion'>"
  	 	+ "<p class='titulo-relacionadas-coleccion'>"+ translate_string_js('Cartas relacionadas') +"</p>"
  	 	+ "<span class='numeros-relacionadas-coleccion'>" + obj[key].relaciones  + "</span>"

  	 	+ "</div>"
  	 	+ "</div>"
  			 // + "<div class=\"cell small-12\">"

         // + "</div>"
         + "</div>"
         + "</div>";
       }
     }
   }
   if (mostrar_tarjetas_asociadas == "") {
   	mostrar_tarjetas_asociadas = "<span class=\"banner_coleccion_vacia padding-vertical\">"+ translate_string_js('En este momento no tienes ninguna tarjeta guardada en tu colección, puedes agregarlas en las secciones ') +"<a href=\"ver_todas.html\">"+ translate_string_js('Ver todas las tarjetas') +"</a>, <a href=\"al_azar_1.html\">"+ translate_string_js('Carta al azar') +"</a> o <a href=\"utiliza.html\">"+ translate_string_js('Visita guiada') +"</a></span>"
   }
   $("#tarjetas").html(mostrar_tarjetas_asociadas);
 };

 function borrar_coleccion() {
 	localStorage.clear();
 	window.location.href = "index.html";
 }
// Fin manejo de colecciones

// Inicio funciones del home
function mostrar_categorias_home(){
	var categorias = "";
	for ( var i = 0; i < datos.categorias.length; i++) {
		var obj = datos.categorias[i];
		for ( var key in obj) {
			categorias += "<div class=\"cell small-12 medium-6 categoria\" data-aos=\"fade-up\" data-aos-duration=\"2000\" data-aos-anchor=\".categorias\"><a href='utiliza.html?categoria="+ key +"'><img class=\"imagen-categoria\" src=\"img/"+ obj[key].imagen +"\"><p class=\"texto-categoria texto-blanco\">"+obj[key].nombre +"</p></a></div>"
		};
	};
	$("#categorias").html(categorias);
}
// Fin funciones del home

// Inicio funciones de utiliza

function procesar_utiliza(){
	if ($.urlParam('categoria') >= 1){
		id_categoria = $.urlParam('categoria');
		if (typeof datos.categorias[0][id_categoria] !== 'undefined') {
			$('#lista_categorias').addClass('hide');
			categoria = datos.categorias[0][id_categoria];
			mostrar_categoria_utiliza(id_categoria);
		} else {
			$('#detalle_categoria').addClass('hide');
		};
	} else {
		$('#detalle_categoria').addClass('hide');
	};
};

function volver_a_categorias() {
	$('#lista_categorias').removeClass('hide');
	$('#lista_categorias').addClass('show');
	$('#detalle_categoria').removeClass('show');
	$('#detalle_categoria').addClass('hide');
}

function mostrar_categoria_utiliza(id_categoria){
	categoria = datos.categorias[0][id_categoria];
	det_categoria = "  <div class=\"grid-x padding-vertical padding-horizontal-1 align-bottom text-left detalle-categoria\" style=\"background: url(img/" + categoria.imagen + ")\">"
	+ "<div class=\"cell small-6 texto-blanco\">"
	+ "<h3 class=\"texto-blanco\">" + categoria.nombre + "</h3>" + categoria.desarrollo
	+ "<select id=\"preguntas_utiliza\" onchange=\"accionar_pregunta_utiliza()\"><option disabled selected>"+ translate_string_js('Selecciona una pregunta') +"</option>";
	for ( var i = 0; i < datos.categorias[0][id_categoria].preguntas.length; i++) {
		var obj = datos.categorias[0][id_categoria].preguntas[i];
		for ( var key in obj) {
			det_categoria += "<option value=\""+ id_categoria +"-"+ key +"\"> " +obj[key].pregunta +"</option>"
		};
	};
	det_categoria += "</select>"
	+ "<a href=\"#\" class=\"link-texto-blanco\" onclick=\"volver_a_categorias();\">"+ translate_string_js('Volver a categorías') +"</a>"
	+ "</div>"
	+ "<div class=\"cell small-6 texto-azul miralas-todas-peq\">"
	+ "</div>";
	$("#detalle_interno_categoria").html(det_categoria);
}

function accionar_pregunta_utiliza(){
	categoria_pregunta = $('#preguntas_utiliza').val().split("-");
	mostrar_cartas_pregunta_utiliza(categoria_pregunta[0],categoria_pregunta[1]);
	$("body,html").animate({
		scrollTop: $("#titulo-tarjetas-utiliza").offset().top
      }, 800 //speed
      );
}

// Selección de pregunta y búsqueda de cartas
function mostrar_cartas_pregunta_utiliza(categoria,pregunta){
	var mostrar_tarjetas_asociadas = "";
	var tarjetas_asociadas = String(datos.categorias[0][categoria].preguntas[0][pregunta].tarjetas);
	$('#titulo-tarjetas-utiliza').html(datos.categorias[0][categoria].preguntas[0][pregunta].pregunta);
	$('#titulo-tarjetas-utiliza').removeClass('hide');
	$('#footer-tarjetas-utiliza').removeClass('hide');
	tarjetas_asociadas = tarjetas_asociadas.split(",");
	coleccion = JSON.parse(localStorage.getItem("coleccion"));
	for (var i in tarjetas_asociadas) {
		numero_tarjeta = parseInt(tarjetas_asociadas[parseInt(i)])
		var obj = coleccion[numero_tarjeta-1];
   //console.log(numero_tarjeta);
   for ( var key in obj) {
   	if (obj[key].seleccionada == 0) {
   		clase_icono = "fi-star";
   	} else {
   		clase_icono = "fi-check";
   	};
   	mostrar_tarjetas_asociadas += "<div class=\"small-3 carta-utiliza carta-utiliza-"+ obj[key].tipo_carta + " scene-flip\">"
   	+ "<div class=\"card-flip\">"
   	+ "<div class=\"grid-x grid-margin-x card__face card__face--front\" id=\"portada-tarjeta-utiliza-" + key + "\">"
   	+ "<div class=\"cell small-12 text-center\"><img class=\"utiliza-imagen-tarjeta\" src=\"img/sf"+ obj[key].arte +"\"></div>"
   	+ "</div>"
   	+ "<div class=\"grid-x grid-margin-x card__face card__face--back\" id=\"reverso-tarjeta-utiliza-" + key + "\">"
   	+ "<div class=\"header-card grid-x\">"
   	+ "<div class=\"small-9\">"
   	+ "<p class=\"titulo-carta\">"+ obj[key].titulo +"</p>"
   	+ "</div>"
   	+ "<div class=\"small-3\">"
   	+ "<span class=\"numero-carta\">" + key + "</span>"
   	+ "<i onclick=\"marcar_tarjeta(" + key + ");\" class=\"fi-heart clase-0 icono-favorito marcar_" + key + " favorite\"></i>"
   	+ "</div>"
   	+ "</div>"
   	+ "<div class=\"cell small-12\">"
   	+ "<p class=\"descripcion-carta\">"+ text_truncate(obj[key].descripción) +"</p>"
   	+ "<a class=\"button card-button small\" href=\"#\" onclick=\"popup_tarjeta(" + key + ");\">"+ translate_string_js('Leer más') +"</a>"
   	+ "</div>"
   	+ "	</div>"
   	+ "</div>"
   	+ "</div>"
   }
 }
 $("#tarjetas-microsd").html(mostrar_tarjetas_asociadas);
 $('.card-flip').click(function(){
 	$(this).toggleClass('is-flipped');
 });
};

function voltear_tarjeta(tarjeta) {
	$('#portada-tarjeta-utiliza-' + tarjeta).toggleClass('hide');
	$('#reverso-tarjeta-utiliza-' + tarjeta).toggleClass('hide');
}

// Procesamiento de todas las cartas
function mostrar_cartas_ver_todas(){
	var mostrar_tarjetas_asociadas = "";
	var tarjetas_asociadas = String(datos.tarjetas);
	coleccion = JSON.parse(localStorage.getItem("coleccion"));
	for (var i in tarjetas_asociadas) {
		numero_tarjeta = i;
		var obj = coleccion[numero_tarjeta-1];
   //console.log(numero_tarjeta);
   for ( var key in obj) {
   	if (obj[key].seleccionada == 0) {
   		clase_icono = "fi-star";
   	} else {
   		clase_icono = "fi-check";
   	};
   	mostrar_tarjetas_asociadas += "<div class=\"small-3 carta-utiliza carta-utiliza-"+ obj[key].tipo_carta + " scene-flip\">"
   	+ "<div class=\"card-flip\">"
   	+ "<div class=\"grid-x grid-margin-x card__face card__face--front\" id=\"portada-tarjeta-utiliza-" + key + "\">"
   	+ "<div class=\"cell small-12 text-center\"><img class=\"utiliza-imagen-tarjeta\" src=\"img/sf"+ obj[key].arte +"\"></div>"
   	+ "</div>"
   	+ "<div class=\"grid-x grid-margin-x card__face card__face--back\" id=\"reverso-tarjeta-utiliza-" + key + "\">"
   	+ "<div class=\"header-card grid-x\">"
   	+ "<div class=\"small-9\">"
   	+ "<p class=\"titulo-carta\">"+ obj[key].titulo +"</p>"
   	+ "</div>"
   	+ "<div class=\"small-3\">"
   	+ "<span class=\"numero-carta\">" + key + "</span>"
   	+ "<i onclick=\"marcar_tarjeta(" + key + ");\" class=\"fi-heart clase-0 icono-favorito marcar_" + key + " favorite\"></i>"
   	+ "</div>"
   	+ "</div>"
   	+ "<div class=\"cell small-12\">"
   	+ "<p class=\"descripcion-carta\">"+ text_truncate(obj[key].descripción) +"</p>"
   	+ "<a class=\"button card-button small\" href=\"#\" onclick=\"popup_tarjeta(" + key + ");\">"+ translate_string_js('Leer más') +"</a>"
   	+ "</div>"
   	+ "	</div>"
   	+ "</div>"
   	+ "</div>"
   }
 }
 $("#tarjetas-microsd").html(mostrar_tarjetas_asociadas);
};

function tarjeta_al_azar(){
	var obj = datos.tarjetas[Math.floor(Math.random()*datos.tarjetas.length)];
	for ( var key in obj) {
		numero_carta_azar = key;
	}
	var contenido_tarjeta = "";
	var imagen_tarjeta = "";
	var relaciones_tarjeta = "";
	var tarjeta_seleccionada = obj[numero_carta_azar];
	var coleccion = JSON.parse(localStorage.getItem("coleccion"));
	var tarjeta_seleccionada = coleccion[numero_carta_azar-1][numero_carta_azar];
	if (tarjeta_seleccionada.seleccionada == 0) {
		clase_icono = "fi-heart favorite";
	} else {
		clase_icono = "fi-heart favorite-selected";
	};
	$('#tarjeta_principal_azar').addClass('carta-'+tarjeta_seleccionada["tipo_carta"]);
	$('#seccion_tarjeta_azar').addClass('fondo-carta-'+tarjeta_seleccionada["tipo_carta"]);
	contenido_tarjeta += "<span class=\"titulo-carta\">"+ tarjeta_seleccionada["titulo"] + "</span><i onclick=\"marcar_tarjeta("+ numero_carta_azar +");\" class=\""+ clase_icono +" icono-favorito marcar_"+ numero_carta_azar +"\"></i>\n\n" +"<p class=\"texto-azul descripcion-carta-azar\">"+ tarjeta_seleccionada["descripción"] + "</p>";
	relaciones_tarjeta += "";
	tarjetas_asociadas_l = String(tarjeta_seleccionada["relaciones"]);
	tarjetas_asociadas = tarjetas_asociadas_l.split(",");
	for (var i in tarjetas_asociadas) {
    //console.log(tarjetas_asociadas[parseInt(i)]);
    numero_tarjeta = parseInt(tarjetas_asociadas[parseInt(i)])
    var obj = datos.tarjetas[numero_tarjeta-1];
    //	console.log(numero_tarjeta);
    for ( var key in obj) {
    	relaciones_tarjeta += "<div class='card-2 carta carta-"+ obj[key].tipo_carta +"' onclick='popup_tarjeta("+ key +");'><img src='img/th"+ obj[key].arte +"'>"
    	+ "<div class='card-section'>"
    	+ "<p class='titulo-carta'>"+ key +") "+ obj[key].titulo +"</p>"
    	+ "</div>"
    	+ "</div>";
    }
  }
  imagen_tarjeta += "<img src='img/" + tarjeta_seleccionada["arte"] + "'>"
  $("#imagen_carta_azar").html(imagen_tarjeta);
  $("#contenido_carta_azar").html(contenido_tarjeta);
  $("#relaciones_carta_azar").html(relaciones_tarjeta);
}
