## Sitio web MicroSD

https://derechosdigitales.org/microsd/

## Build

Debido a que por cómo estaba construído no fue posible integrar la carga de los archivos de idioma como variables es que se tomó la opción de generar múltiples sitios copiados desde la carpeta "default". Para considerar en caso de modificaciones:

- Todas las modificaciones se deben realizar en la carpeta "default".
- Luego de realizadas las modificaciones se debe ejecutar el script "build.sh" que generará un directorio llamada "microsd" y dentro un directorio por cada lenguaje.
- Antes de realizar un commit se debe eliminar la carpeta "microsd".

** Debo revisar como guardar la colección si es que no cambio de idioma.