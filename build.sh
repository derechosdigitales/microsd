#!/bin/bash

# Genera copias de la carpeta "default" para cada idioma.

langs=('en' 'es' 'pt')

folder=microsd
zfile=MicroSD.zip

rm -rf $folder
mkdir $folder

for i in "${langs[@]}"
do
	cp index.html $folder
	cp -r default/ $folder/$i
	cd $folder/$i
	cp lang/$i.json lang.json
	cd ../../
done

zip -r -q $zfile $folder


for i in "${langs[@]}"
do
	cp $zfile $folder/$i
done

rm $zfile